# Project 2

Report and code for the project 2 of the ML@SNS exam 2023.

## Artifacts

You can find the pdf [here](https://gitlab.com/teampano/machine-learning/-/jobs/artifacts/main/raw/project2.pdf?job=compile_pdf).

You can download the package to upload for the exam [here](https://gitlab.com/teampano/machine-learning/-/jobs/artifacts/main/raw/INTROML_PJ2_TeamPano.zip?job=compile_pdf).