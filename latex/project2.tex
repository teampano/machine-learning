\documentclass[a4paper, notitlepage]{article}

% formatting and fontface
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[italian]{babel}
\usepackage{anyfontsize}
\usepackage{xcolor}
\usepackage{expl3}
\usepackage{siunitx}
\usepackage{hyperref}
\usepackage{geometry}

%bibliography
\usepackage[autostyle,italian=guillemets]{csquotes}
\usepackage[style=numeric]{biblatex} % ,backend=biber
\addbibresource{biblio.bib}

%symbols
\usepackage{amsmath, amsthm, amsfonts, mathtools, mathrsfs, wasysym, accents, commath}

% figure handling
\usepackage[font=small]{caption}
\usepackage{subcaption}
\usepackage{multicol, multirow, placeins}
\usepackage{booktabs}
\usepackage{minted}

% document title
\author{Luca Francesco D'Alessandro, Giorgio Mangioni, Alessio Marchetti}
\title{\bfseries Analisi del dataset PlantVillage attraverso Reti Neurali}
\date{\today}


\begin{document}

\setlength\parindent{0pt}

\maketitle

\begin{abstract}
Per la seconda consegna abbiamo scelto di studiare il dataset di immagini \textit{Plant Disease Detection}, reperibile online\footnote{\url{https://www.kaggle.com/datasets/vipoooool/new-plant-diseases-dataset}} e utilizzato in letteratura per vari studi, e.g. \cite{mohanty2016using}.  Abbiamo allenato una rete neurale a riconoscere il tipo di pianta e le eventuali malattie della pianta, guardando solo una foto di una foglia. L'architettura è una versione modificata di ResNet, che utilizza come funzioni di attivazione delle funzioni ReLU.  Un secondo modello, ispirato a quello proposto in \cite{coloraware}, utilizza l'encoding LAB per le immagini in input, in  modo da passare il canale L e i canali AB  a due branch separate della rete. Per l'ultimo modello abbiamo modificato la funzione di loss aggiungendo un secondo target in cui si penalizza maggiormente l'errore di identificazione della pianta indipendentemente dall'identificazione o meno di una malattia. Tutti e tre i modelli danno risultati significativi, con un'accuratezza poco superiore al \qty{97}{\percent}.
	% TODO questa segnati che va sistemata con i dati veri
\end{abstract}

\section{Analisi del dataset}

Il dataset contiene \num{87867} immagini RGB di dimensione \texttt{256x256x3}; esse raffigurano foglie di varie piante su sfondo neutro, appartenenti sia a piante sane sia a piante malate. Le malattie che ciascuna pianta può presentare sono specificate nella Tabella \ref{tab:classi}.
In totale sono presenti \num{14} tipi di piante diverse; per tutte tranne due (\textit{Orange} e \textit{Squash}) sono presenti immagini della pianta sana (\texttt{health}). Le malattie totali sono invece \num{26}, dunque vi sono \num{38} classi uniche in totale.
Il dataset è già suddiviso in due dataset di training e validation in rapporto \qty{80}{\percent}/\qty{20}{\percent}; in entrambi i dataset le piante sane costituiscono il \qty{24.1}{\percent} del totale. Inoltre è presente un piccolo dataset di test, composto da \num{33} immagini.
In Figura \ref{fig:barplot-class} è presentata la distribuzione delle classi. Notiamo che il dataset è sostanzialmente bilanciato fra le classi, e quindi non presenta alcun bias significativo; tuttavia alcune piante (ad esempio il pomodoro) sono più rappresentate rispetto alle altre. Ciò non influisce sulla classificazione nei primi due modelli da noi implementati, che sono stati allenati a riconoscere sia la pianta sia l'eventuale malattia e che considerano errore ogni caso in cui una delle due non è corretta; sarà invece rilevante nel terzo, in cui la funzione di loss penalizza meno i casi in cui il modello predice la pianta ma sbaglia a riconoscerne lo stato di salute.



\begin{table}[ht]
	\renewcommand{\arraystretch}{1.15} % Default value: 1
	\begin{tabular}{p{0.2\linewidth}p{0.8\linewidth}}
	\toprule
	\textbf{Piante} & \textbf{Malattie} \\
	\midrule
	Tomato & Late blight, Early blight, Septoria leaf spot, Yellow leaf curl virus, Bacterial Spot, Target spot, Tomato Mosaic virus, Leaf mold, Spider mites. \\
	Grape & Isariopsis Leaf Spot, Black rot, Esca (Black Measles),  \\
	Orange & Haunglongbing.  \\
	Soybean &  \\
	Squash & Powdery mildew.  \\
	Potato & Late blight, Early blight. \\
	Corn (maize) & Northern Leaf Blight, Cercospora leaf spot, Common rust.  \\
	Strawberry & Leaf scorch. \\
	Peach & Bacterial spot.  \\
	Apple & Scab, Black rot, Cedar apple rust.  \\
	Blueberry &  \\
	Cherry & Powdery mildew.  \\
	Bell Pepper & Bacterial spot. \\
	Raspberry &  \\
	\bottomrule  
	\end{tabular}
	\caption{Elenco delle piante e relative malattie.}
	\label{tab:classi}
\end{table}

\begin{figure}[ht]
	\includegraphics[width=\textwidth]{./images/image_per_class_barplot.png}
	\caption{Istogramma del numero di immagini, suddivise per classe. Due classi corrispondenti alla stessa pianta hanno lo stesso colore.}
	\label{fig:barplot-class}
\end{figure}

% FIGURA QUA QUADROTTI DI ROBE



\section{Modelli}
In tutti e tre i modelli da noi implementati, le immagini vengono dapprima caricate sulla GPU con un dataloader, e poi passate alla rete in minibatch da \num{32} elementi, che vengono processati insieme prima di svolgere il training step. L'architettura di base della rete è sempre una versione semplificata del modello \texttt{ResNet} (proposto per la prima volta in \cite{he2016deep}), ottenuta ripetendo il seguente blocco di layer:
\begin{enumerate}
\item un layer di normalizzazione della batch, in cui ogni neurone riscala linearmente il proprio input in modo che esso abbia media nulla e varianza \num{1} al variare degli elementi della batch;
\item un layer di convoluzione con \num{32} canali in entrata e in uscita, con kernel di taglia \texttt{3x3}, stride \num{1} e padding \num{1};
\item un layer di attivazione ReLU;
\item un layer di convoluzione con \num{32} canali in entrata e in uscita, con kernel di taglia \texttt{3x3}, stride \num{1} e padding \num{1};
\item un layer di normalizzazione della batch;
\item una connessione residuale, che somma all'output del precedente layer l'input dell'intero blocco: in questo modo, in fase di backpropagation, il gradiente avrà norma vicina a \num{1}, cosa che contribuisce a velocizzare la convergenza del modello;
\item un layer di attivazione ReLU.
\end{enumerate}


Riportiamo di seguito il codice del blocco base:
\begin{minted}{python}
class ResidualBlock(nn.Module):
    def __init__(self, in_channels, out_channels):
        super().__init__()
        self.pipe = nn.Sequential(
            nn.BatchNorm2d(in_channels),
            nn.Conv2d(in_channels=32, out_channels=32, 
            	kernel_size=3, stride=1, padding=1),
            nn.ReLU(),
            nn.Conv2d(in_channels=32, out_channels=32,
            	kernel_size=3, stride=1, padding=1),
            nn.BatchNorm2d(out_channels),
        )
        self.activation = nn.ReLU() 
        
    def forward(self, x):
        out = self.pipe(x)
        out = out + x
        out = self.activation(out)
        return out
\end{minted}

\begin{figure}[ht]
	\centering
	\includegraphics[width=\textwidth]{./images/ResBlock.pdf}
	\caption{Rappresentazione schematica del \texttt{ResidualBlock}.}
	\label{fig:ResBlock}
\end{figure}


Abbiamo implementato i seguenti tre modelli:
\begin{enumerate}
\item Un modello ``vanilla'', in cui il blocco base viene ripetuto cinque volte;
\item Un secondo modello in cui le immagini vengono dapprima convertite dal formato RGB nel formato LAB; dopodiché il canale L, che tiene conto della luminosità dell'immagine, e i due canali AB, che rappresentano i colori, vengono passati a due branche separate della rete, che si ricongiungono successivamente. Tale scelta è stata fatta per cercare di rendere il risultato indipendente dalla luminosità totale dell'immagine e quanto più vicino possibile a come le foglie vengono percepite dall'occhio umano, come spiegato in \cite{coloraware}.
\item Un terzo modello, che differisce dal primo solo per la funzione di loss. Quest'ultima penalizza maggiormente i casi in cui l'architettura classifica erroneamente il tipo di pianta rispetto ai casi in cui predice correttamente la pianta ma non riconosce l'eventuale malattia. 
\end{enumerate}

\subsection{Modello 1}
Il primo modello è composto dai seguenti layer:
\begin{enumerate}
\item un layer di convoluzione con kernel \texttt{3x3}, che riceve in entrata i tre canali RGB e ha \num{8} canali in uscita;
\item un layer di MaxPooling \texttt{4x4};
\item un layer di attivazione ReLU;
\item un layer di normalizzazione della batch;
\item un layer di convoluzione, sempre con kernel \texttt{3x3}, che aumenta il numero di canali a \num{32};
\item un layer di MaxPooling \texttt{2x2};
\item un layer di attivazione ReLU;
\item il \texttt{ResidualBlock}, ripetuto cinque volte;
\item un layer di convoluzione con kernel \texttt{1x1}, che riduce il numero di canali a \num{16};
\item un layer di MaxPooling \texttt{2x2};
\item un layer che appiattisce le immagini, trasformandole in array di lunghezza \num{4096} (dato che, dopo essere passate per i vari layer di pooling, le immagini sono ora in formato \texttt{16x16} e ciascun pixel ha \num{16} canali);
\item un layer lineare totalmente connesso, il cui output è un vettore di probabilità di lunghezza \num{38} e la cui entrata i-esima corrisponde alla probabilità che l'immagine vada classificata nella classe i-esima.
\end{enumerate}

I layer prima del \text{ResidualBlock}, soprattutto quelli di MaxPooling, sono stati aggiunti con l'obiettivo di ridurre la risoluzione dell'immagine, per velocizzare i tempi di allenamento della rete. Similmente, i layer conclusivi servono a ridurre sia i canali di ciascun pixel sia la risoluzione delle immagini, in modo da ridurre il numero di parametri del layer lineare che esegue la classificazione. In totale il modello ha \num{251910} parametri, di cui \num{155686} solo nel layer lineare finale.

Di seguito riportiamo il codice del primo modello:

\begin{minted}{python}
class Model(nn.Module):
    def __init__(self, channels):
        super().__init__()
        layers = [
            nn.Conv2d(in_channels=3, out_channels=8,
            	kernel_size=3, stride=1, padding=1),
            nn.MaxPool2d(4),
            nn.ReLU(),
            nn.BatchNorm2d(8),
            nn.Conv2d(in_channels=8, out_channels=channels,
            	kernel_size=3, stride=1, padding=1),
            nn.MaxPool2d(2),
            nn.ReLU(),
        ]
        for _ in range(5):
            layers.append(ResidualBlock(channels, channels))
        final = nn.Sequential(
            nn.Conv2d(in_channels=channels, out_channels=16,
            	kernel_size=1, stride=1, padding=0),
            nn.MaxPool2d(2),
            nn.Flatten(),
            nn.Linear(16*16*16, num_diseases)
        )
        layers.append(final)
        self.layers = nn.Sequential(*layers)

    def forward(self, x):
        return self.layers(x)
\end{minted}

\begin{figure}[ht]
	\includegraphics[width=\textwidth]{./images/Modello1.pdf}
	\caption{Rappresentazione schematica del primo modello. Prima dell'ultimo layer lineare le immagini vengono trasformate in array.}
	\label{fig:Modello1}
\end{figure}


Il modello è stato allenato per \num{24} epoche, in ciascuna delle quali l'architettura ha visionato tutte le immagini. La funzione di loss scelta per questo modello è la cross-entropy tra l'output e l'etichetta dell'immagine. Come algoritmo di ottimizzazione abbiamo scelto \texttt{Adam}, impostando un parametro di gradient clipping per evitare l'esplosione del gradiente durante la backpropagation. Abbiamo inoltre richiesto che il learning rate variasse in modo adattivo durante la fase di training, rimanendo sempre minore di \num{e-3}. L'accuratezza di questo modello sul validation set è stata del \qty{97.15}{\percent}.

\subsection{Modello 2}
Le coordinate RGB di un pixel sono altamente correlate, in quanto le variazioni di intensità indotte da cambi di luminosità o di texture modificano i tre canali in modo proporzionale \cite{RGBcorrelate}. Per questo motivo, ispirati dall'articolo \cite{coloraware}, nel secondo modello abbiamo dapprima convertito le immagini nel formato CieLAB. Quest'ultimo è uno spazio di colori a tre coordinate: la prima (L) è la luminosità apparente, che cattura le proprietà ``acromatiche'' dell'immagine (come le texture, le forme ecc.); le altre due (AB) sono coordinate cartesiane nello spazio dei quattro ``colori unici'' della percezione visiva, che sono il rosso, il verde, il giallo e il blu.

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.35\textwidth]{./images/colorcircle.png}
	\caption{Il ``cerchio dei colori unici'', il cui centro corrisponde al bianco e nel quale due punti sono antipodali se corrispondono a colori complementari. I canali AB corrispondono alle coordinate cartesiane in questo cerchio.}
	\label{fig:colorcircle}
\end{figure}

\begin{figure}[ht]
	\centering
	\includegraphics[width=.3915\textwidth]{./images/cielab_top.png}
	\hfill
	\includegraphics[width=.5595\textwidth]{./images/cielab_side.png}
	\caption{Lo spazio CieLAB, visto dall'alto (prima immagine) e da davanti (seconda immagine). Rimandiamo a \href{``https://en.wikipedia.org/wiki/CIELAB_color_space''}{Wikipedia} per una descrizione più precisa di questo spazio.}
	\label{fig:cielab}
\end{figure}

Abbiamo dunque costruito un modello in cui il canale L e i canali AB vengono passati a due rami separati dell'architettura, che successivamente si ricongiungono. Le coordinate AB non devono essere separate, in quanto le informazioni cromatiche dei pixel sono contenute in entrambe le coordinate simultaneamente. I due rami, pressoché identici, sono ottenuti ripetendo tre volte il seguente blocco di layer, denominato \texttt{Basic\char`_Convolution} nel nostro codice:

\begin{enumerate}
\item un layer di convoluzione, con kernel di taglia \texttt{3x3} e stride variabile;
\item un layer di normalizzazione della batch;
\item un layer di attivazione ReLU.
\end{enumerate}

Al termine dei tre blocchi, in entrambi i rami le immagini passano per un layer di MaxPooling \texttt{2x2}. Riportiamo qui il codice del blocco di convoluzione e dei due canali:

\begin{minted}{python}
class Basic_Convolution(nn.Module):
    def __init__(self, layer, out_channels):
        super().__init__()
        self.conv = layer
        self.normalization = nn.BatchNorm2d(out_channels)
        self.activation = nn.ReLU()

    def forward(self, x):
        t = self.conv(x)
        t = self.normalization(t)
        return self.activation(t)

class Path_L_Block(nn.Module):
    def __init__(self, hyperparameter_x):
        super().__init__()
        out_channels = hyperparameter_x // 2
        self.conv1   = Basic_Convolution(nn.Conv2d(in_channels=1,
        		out_channels=out_channels, kernel_size=3,
        		stride=2, padding=0), out_channels)
        self.conv2   = Basic_Convolution(nn.Conv2d(in_channels=out_channels,
        		out_channels=out_channels, kernel_size=3,
        		stride=1, padding=1), out_channels)
        self.conv3   = Basic_Convolution(nn.Conv2d(in_channels=out_channels,
        		out_channels=hyperparameter_x, kernel_size=3,
        		stride=1, padding=1), hyperparameter_x)
        self.pooling = nn.MaxPool2d(2)

    def forward(self, x):
        t = self.conv1(x)
        t = self.conv2(t)
        t = self.conv3(t)
        t = self.pooling(t)
        return t

class Path_AB_Block(nn.Module):
    def __init__(self, hyperparameter_x):
        super().__init__()
        out_channels = 32 - hyperparameter_x // 2
        self.conv1   = Basic_Convolution(nn.Conv2d(in_channels=2,
        		out_channels=out_channels, kernel_size=3,
        		stride=2, padding=0), out_channels)
        self.conv2   = Basic_Convolution(nn.Conv2d(in_channels=out_channels,
        		out_channels=out_channels, kernel_size=3,
        		stride=1, padding=1), out_channels)
        self.conv3   = Basic_Convolution(nn.Conv2d(in_channels=out_channels,
        		out_channels=64 - hyperparameter_x, kernel_size=3,
        		stride=1, padding=1), 64 - hyperparameter_x)
        self.pooling = nn.MaxPool2d(2)

    def forward(self, x):
        t = self.conv1(x)
        t = self.conv2(t)
        t = self.conv3(t)
        t = self.pooling(t)
        return t
\end{minted}

Dal ramo L escono immagini i cui pixel hanno \num{16} canali, mentre dal blocco AB le immagini escono a \num{48} canali.  Più precisamente, nel nostro codice la distribuzione dei canali in uscita tra i due rami dipende dall'iperparametro \texttt{hyperparameter\char`_x}, che abbiamo poi impostato in modo che la componente L dell'immagine pesasse per il \qty{25}{\percent} nella classificazione della foglia e che il totale dei canali fosse \num{64}. Ciò ricalca la distribuzione dei canali nel modello di \cite{coloraware} con migliore performance.

Dopo essere passati per i due rami, l'immagine L e l'immagine AB vengono concatenate, così da formare un'unica immagine i cui pixel hanno ora \num{64} canali. Da questo punto il modello è praticamente identico al precedente: l'immagine passa per cinque \text{ResidualBlock} (stavolta con \num{64} canali in entrata e in uscita), un layer di convoluzione \texttt{1x1}, uno di MaxPooling \texttt{2x2} e un layer lineare totalmente connesso. In totale il modello ha \num{974094} parametri, di cui \num{584326} nel layer lineare.

Il codice del modello è dunque il seguente:

\begin{minted}{python}
class Model(nn.Module):
    def __init__(self, channels):
        super().__init__()
        layers = [
            nn.Conv2d(in_channels=3, out_channels=8,
            	kernel_size=3, stride=1, padding=1),
            nn.MaxPool2d(4),
            nn.ReLU(),
            nn.BatchNorm2d(8),
            nn.Conv2d(in_channels=8, out_channels=channels,
            	kernel_size=3, stride=1, padding=1),
            nn.MaxPool2d(2),
            nn.ReLU(),
        ]
        for _ in range(5):
            layers.append(ResidualBlock(channels, channels))
        final = nn.Sequential(
            nn.Conv2d(in_channels=channels, out_channels=16,
            	kernel_size=1, stride=1, padding=0),
            nn.MaxPool2d(2),
            nn.Flatten(),
            nn.Linear(16*16*16, num_diseases)
        )
        layers.append(final)
        self.layers = nn.Sequential(*layers)

    def forward(self, x):
        return self.layers(x)
\end{minted}

\begin{figure}[ht]
	\includegraphics[width=\textwidth]{./images/Modello2.pdf}
	\caption{Rappresentazione schematica del secondo modello. Il simbolo $\equiv$ rappresenta la concatenazione delle due immagini parziali.}
	\label{fig:Modello2}
\end{figure}

Poiché questo modello ha molti più parametri del precedente lo abbiamo allenato solamente per \num{12} epoche, in modo che i tempi di elaborazione fossero all'incirca gli stessi. L'algoritmo di ottimizzazione e i suoi parametri (learning rate, gradient clipping, weight decay) sono gli stessi del modello precedente. Il modello ha un'accuratezza del \qty{96.63}{\percent} sul validation set, leggermente inferiore ai risultati del primo modello.

\subsection{Modello 3}

Il terzo modello differisce dal primo solo per la funzione di loss. Infatti abbiamo considerato le seguenti due funzioni in combinazione lineare: la cross-entropy fra l'output e l'etichetta dell'immagine relativamente alla pianta a cui appartiene, la cross-entropy tra l'output e l'etichetta globale dell'immagine. La combinazione delle due funzioni è regolata da un iperparametro \texttt{plant\char`_disease\char`_tradeoff}, che abbiamo regolato manualmente. 

Di seguito il codice che implementa la funzione di loss globale:

\begin{minted}{python}
plant_prediction = out @ class_to_plant
plant_idx = F.embedding(labels, class_to_plant_idx)
loss_disease = F.cross_entropy(out, labels)
loss_plant   = F.cross_entropy(plant_prediction, plant_idx.reshape(-1))
loss = loss_disease + plant_disease_tradeoff * loss_plant 
\end{minted}

Il modello, analogamente al primo, è stato allenato per 24 epoche. 
Le scelte dell'algoritmo di ottimizzazione, della presenza di un parametro di gradient clipping e della variazione adattiva del learning rate sono rimaste invariate. 
A fronte di queste modifiche, nella predizione della malattia abbiamo ottenuto un'accuratezza sul validation set pari al \qty{97.21}{\percent}, di poco dissimile da quella del primo modello, mentre nella predizione della pianta di appartenenza abbiamo ottenuto un'accuratezza pari a \qty{98.51}{\percent}. 

\section{Conclusioni}

Come si può vedere dalla Figura \ref{fig:val_accuracies}, tutti e tre i modelli hanno precisioni finali superiori al \qty{97}{\percent}, leggermente inferiori alle accuratezze dei modelli proposti in \cite{mohanty2016using} e \cite{coloraware} (superiori al \qty{99}{\percent}).
I modelli raggiungono accuratezze superiori al \qty{90}{\percent} già dopo poche epoche di allenamento (meno di \num{10}).

Notiamo che il secondo modello ha una maggiore precisione rispetto agli altri nelle prime \num{12} epoche, ma che la sua precisione finale è inferiore. Ciò potrebbe essere dovuto al differente numero di epoche di allenamento dei tre modelli, solamente \num{12} per il secondo e invece \num{24} per il primo e il terzo.

Confrontando il primo modello e il terzo, sempre dalla Figura \ref{fig:val_accuracies} notiamo come la loro accuratezza nella distinzione delle malattie abbia sostanzialmente lo stesso andamento e lo stesso valore finale. Dunque la modifica della funzione di loss non ha portato miglioramenti significativi nel riconoscimento delle malattie. Tuttavia, l'accuratezza nella predizione della sola pianta è maggiore rispetto a quella della malattia annessa, come era da aspettarsi.

Infine, tutti e tre i modelli predicono correttamente tutte e \num{33} le immagini del dataset di test.

\begin{figure}[ht]
\centering
	\includegraphics[width=0.8\textwidth]{./images/val_accuracies.png}
	\caption{Accuratezza dei tre modelli sul validation set, al variare delle epoche. Per il modello 2 abbiamo prolungato l'accuratezza registrata nell'ultima epoca, per confrontarla meglio con quelle degli altri due.}
	\label{fig:val_accuracies}
\end{figure}

\printbibliography[title={Riferimenti bibliografici}, heading=bibnumbered]

\end{document}
