import os
import numpy as np
import pandas as pd
import torch
import matplotlib.pyplot as plt
import torch.nn as nn
from torch.utils.data import DataLoader
from PIL import Image
import torch.nn.functional as F
import torchvision.transforms as transforms
from torchvision.datasets import ImageFolder

from datetime import datetime
from tqdm import tqdm

def log(msg):
    with open('log', 'a') as f:
        f.write(msg + '\n')
    print(msg)


while len(tqdm._instances) > 0:
    tqdm._instances.pop().close()

################### PARTE 1: CARICAMENTO DATI ####################

data_dir = "../data/New Plant Diseases Dataset(Augmented)/New Plant Diseases Dataset(Augmented)"
train_dir = data_dir + "/train"
valid_dir = data_dir + "/valid"

num_diseases = 38

# datasets for validation and training
train = ImageFolder(train_dir, transform=transforms.ToTensor())
valid = ImageFolder(valid_dir, transform=transforms.ToTensor()) 

# setting the batch size
batch_size = 32

# Setting the seed value
random_seed = 42
torch.manual_seed(random_seed)

# DataLoaders for training and validation
train_dl = DataLoader(train, batch_size, shuffle=True, num_workers=2, pin_memory=True)
valid_dl = DataLoader(valid, batch_size, num_workers=2, pin_memory=True)

# for moving data to device (CPU or GPU)
def to_device(data, device):
    """Move tensor(s) to chosen device"""
    if isinstance(data, (list,tuple)):
        return [to_device(x, device) for x in data]
    return data.to(device, non_blocking=True)


# for loading in the device (GPU if available else CPU)
class DeviceDataLoader():
    """Wrap a dataloader to move data to a device"""
    def __init__(self, dl, device):
        self.dl = dl
        self.device = device
        
    def __iter__(self):
        """Yield a batch of data after moving it to device"""
        for b in self.dl:
            yield to_device(b, self.device)
        
    def __len__(self):
        """Number of batches"""
        return len(self.dl)

device = torch.device('cuda' if torch.cuda.is_available else 'cpu')


# Moving data into GPU
train_dl = DeviceDataLoader(train_dl, device)
valid_dl = DeviceDataLoader(valid_dl, device)


################### PARTE 2: DEFINIZIONE DEL MODELLO  ###################

class ResidualBlock(nn.Module):
    def __init__(self, in_channels, out_channels):
        super().__init__()
        self.pipe = nn.Sequential(
            nn.BatchNorm2d(in_channels),
            nn.Conv2d(in_channels=in_channels, out_channels=out_channels, kernel_size=3, stride=1, padding=1),
            nn.ReLU(),
            nn.Conv2d(in_channels=out_channels, out_channels=out_channels, kernel_size=3, stride=1, padding=1),
            nn.BatchNorm2d(out_channels),
        )
        self.activation = nn.ReLU() 
        
    def forward(self, x):
        out = self.pipe(x)
        out = out + x
        out = self.activation(out)
        return out

class Model(nn.Module):
    def __init__(self, channels):
        super().__init__()
        layers = [
            nn.Conv2d(in_channels=3, out_channels=8, kernel_size=3, stride=1, padding=1),
            nn.MaxPool2d(4),
            nn.ReLU(),
            nn.BatchNorm2d(8),
            nn.Conv2d(in_channels=8, out_channels=channels, kernel_size=3, stride=1, padding=1),
            nn.MaxPool2d(2),
            nn.ReLU(),
        ]
        for _ in range(5):
            layers.append(ResidualBlock(channels, channels))
        final = nn.Sequential(
            nn.Conv2d(in_channels=channels, out_channels=16, kernel_size=1, stride=1, padding=0),
            nn.MaxPool2d(2),
            nn.Flatten(),
            nn.Linear(16*16*16, num_diseases)
        )
        layers.append(final)
        self.layers = nn.Sequential(*layers)

    def forward(self, x):
        return self.layers(x)
        

################### PARTE 3: TRAINING, VALIDATION, TEST PROCEDURES  ###################

def train(channels):
    torch.cuda.empty_cache()
    history = []

    max_epochs   = 24
    max_lr       = 1e-3
    grad_clip    = 0.05
    weight_decay = 1e-4

    model = Model(channels).to(device)

    optimizer = torch.optim.Adam(model.parameters(), max_lr, weight_decay=weight_decay)
    scheduler = torch.optim.lr_scheduler.OneCycleLR(optimizer, max_lr, epochs=max_epochs, steps_per_epoch=len(train_dl))

    starting_time = datetime.now()


    for epoch in range(max_epochs):
        print(f'Starting epoch {epoch}')
        model.train()
        train_losses = []
        lr_history   = []

        while len(tqdm._instances) > 0:
            tqdm._instances.pop().close()
        bar = tqdm(total=len(train_dl))

        for batch in train_dl:
            images, labels = batch
            out  = model(images)
            loss = F.cross_entropy(out, labels)
            loss.backward()
            train_losses.append(loss)

            nn.utils.clip_grad_value_(model.parameters(), grad_clip)

            optimizer.step()
            optimizer.zero_grad()

            lr_history.append(optimizer.param_groups[0]['lr'])
            scheduler.step()

            bar.update(1)
        bar.close()

        print(f'Starting evaluation of epoch {epoch}')
        train_mean_loss    = torch.stack(train_losses).mean()
        validation_results = evaluate(model, valid_dl)
        val_mean_loss      = validation_results['eval_mean_loss']
        val_mean_accuracy  = validation_results['eval_mean_accuracy']

        time = datetime.now() - starting_time
        
        msg = ''
        msg += f'epoch {epoch}  --  ' 
        msg += f'time: {time}  --  '
        msg += f'train_loss: {train_mean_loss:.4f} - '
        msg += f'val_loss: {val_mean_loss:.4f} - '
        msg += f'val_accuracy: {val_mean_accuracy:.4f}'
        log(msg)

    return {
        'model': model,
    }



def evaluate(model, dataset):
    model.eval()
    with torch.no_grad():
        losses     = []
        accuracies = []

        for batch in dataset:
            images, labels = batch
            out      = model(images)
            loss     = F.cross_entropy(out, labels)
            accuracy = compute_accuracy(out, labels)

            losses.append(loss)
            accuracies.append(accuracy)

        mean_loss     = torch.stack(losses).mean()
        mean_accuracy = torch.stack(accuracies).mean()
    return {
        'eval_mean_loss':     mean_loss,
        'eval_mean_accuracy': mean_accuracy
    }


# for calculating the accuracy
def compute_accuracy(outputs, labels):
    _, preds = torch.max(outputs, dim=1)
    return torch.tensor(torch.sum(preds == labels).item() / len(preds))

    

def test(model):
    test_dir = "../data/New Plant Diseases Dataset(Augmented)/test"
    test = ImageFolder(test_dir, transform=transforms.ToTensor())
    test_images = sorted(os.listdir(test_dir + '/test')) # since images in test folder are in alphabetical order
    test_dl = DataLoader(test, batch_size, num_workers=2, pin_memory=True)

    results = evaluate(model, test_dl)
    mean_loss     = results['val_mean_loss']
    mean_accuracy = results['val_mean_accuracy']

    log(f'test_mean_loss: {mean_loss:.4f}  -- test_mean_accuracy: {mean_accuracy:.4f}')

################# PARTE 4: RUN EFFETTIVA ####################

train_results = train(32)
model = train_results['model']


#######################   RISULTATI    ######################
# epoch  0  --  time: 0:02:30.900949  --  train_loss: 1.3624 - val_loss: 0.6544 - val_accuracy: 0.8006
# epoch  1  --  time: 0:03:50.351513  --  train_loss: 0.5873 - val_loss: 0.4940 - val_accuracy: 0.8402
# epoch  2  --  time: 0:05:12.071904  --  train_loss: 0.4430 - val_loss: 0.6970 - val_accuracy: 0.7971
# epoch  3  --  time: 0:06:32.697564  --  train_loss: 0.3403 - val_loss: 0.4547 - val_accuracy: 0.8600
# epoch  4  --  time: 0:07:51.451242  --  train_loss: 0.2802 - val_loss: 0.3078 - val_accuracy: 0.9089
# epoch  5  --  time: 0:09:14.006538  --  train_loss: 0.2409 - val_loss: 0.3055 - val_accuracy: 0.9117
# epoch  6  --  time: 0:12:09.822993  --  train_loss: 0.2085 - val_loss: 1.0260 - val_accuracy: 0.7892
# epoch  7  --  time: 0:23:47.311272  --  train_loss: 0.1730 - val_loss: 0.3917 - val_accuracy: 0.9059
# epoch  8  --  time: 0:28:59.044657  --  train_loss: 0.1408 - val_loss: 0.3309 - val_accuracy: 0.9176
# epoch  9  --  time: 0:29:42.424648  --  train_loss: 0.1198 - val_loss: 0.2767 - val_accuracy: 0.9317
# epoch 10  --  time: 0:30:26.498517  --  train_loss: 0.0923 - val_loss: 0.3203 - val_accuracy: 0.9267
# epoch 11  --  time: 0:31:11.312087  --  train_loss: 0.0762 - val_loss: 0.3044 - val_accuracy: 0.9324
# epoch 12  --  time: 0:31:56.001703  --  train_loss: 0.0613 - val_loss: 0.2823 - val_accuracy: 0.9357
# epoch 13  --  time: 0:32:39.970462  --  train_loss: 0.0481 - val_loss: 0.2577 - val_accuracy: 0.9394
# epoch 14  --  time: 0:33:23.464644  --  train_loss: 0.0356 - val_loss: 0.2731 - val_accuracy: 0.9376
# epoch 15  --  time: 0:34:07.532711  --  train_loss: 0.0220 - val_loss: 0.2051 - val_accuracy: 0.9565
# epoch 16  --  time: 0:34:50.669110  --  train_loss: 0.0172 - val_loss: 0.2624 - val_accuracy: 0.9407
# epoch 17  --  time: 0:35:35.380756  --  train_loss: 0.0107 - val_loss: 0.1582 - val_accuracy: 0.9653
# epoch 18  --  time: 0:36:19.800656  --  train_loss: 0.0054 - val_loss: 0.1491 - val_accuracy: 0.9675
# epoch 19  --  time: 0:37:03.121022  --  train_loss: 0.0033 - val_loss: 0.1382 - val_accuracy: 0.9695
# epoch 20  --  time: 0:37:45.895262  --  train_loss: 0.0015 - val_loss: 0.1218 - val_accuracy: 0.9708
# epoch 21  --  time: 0:38:32.157004  --  train_loss: 0.0009 - val_loss: 0.1253 - val_accuracy: 0.9701
# epoch 22  --  time: 0:39:32.971060  --  train_loss: 0.0007 - val_loss: 0.1176 - val_accuracy: 0.9712
# epoch 23  --  time: 0:40:45.033633  --  train_loss: 0.0007 - val_loss: 0.1153 - val_accuracy: 0.9715

#######################   RISULTATI    ######################
# Total params: 251,910
# Params in Linear: 155,686
