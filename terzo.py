import os
import numpy as np
import pandas as pd
import torch
import matplotlib.pyplot as plt
import torch.nn as nn
from torch.utils.data import DataLoader
from PIL import Image
import torch.nn.functional as F
import torchvision.transforms as transforms
from torchvision.datasets import ImageFolder

from datetime import datetime
from tqdm import tqdm

def log(msg):
    with open('log', 'a') as f:
        f.write(msg + '\n')
    print(msg)


while len(tqdm._instances) > 0:
    tqdm._instances.pop().close()

################### PARTE 1: CARICAMENTO DATI ####################

data_dir = "../data/New Plant Diseases Dataset(Augmented)/New Plant Diseases Dataset(Augmented)"
train_dir = data_dir + "/train"
valid_dir = data_dir + "/valid"

num_diseases = 38

# datasets for validation and training
train = ImageFolder(train_dir, transform=transforms.ToTensor())
valid = ImageFolder(valid_dir, transform=transforms.ToTensor()) 

# associate each disease to its plant

plants = []
for name, _ in train.class_to_idx.items():
    plant_name = name.split('___')[0]
    if plant_name not in plants:
        plants.append(plant_name)
class_to_plant = np.zeros((38, len(plants)), dtype=np.float32)
class_to_plant_idx = np.zeros(38, dtype=np.int64)
for name, idx in train.class_to_idx.items():
    plant_name = name.split('___')[0]
    plant_idx = plants.index(plant_name)
    class_to_plant[idx, plant_idx] = 1
    class_to_plant_idx[idx] = plant_idx

# setting the batch size
batch_size = 32

# Setting the seed value
random_seed = 42
torch.manual_seed(random_seed)

# DataLoaders for training and validation
train_dl = DataLoader(train, batch_size, shuffle=True, num_workers=2, pin_memory=True)
valid_dl = DataLoader(valid, batch_size, num_workers=2, pin_memory=True)

# for moving data to device (CPU or GPU)
def to_device(data, device):
    """Move tensor(s) to chosen device"""
    if isinstance(data, (list,tuple)):
        return [to_device(x, device) for x in data]
    return data.to(device, non_blocking=True)


# for loading in the device (GPU if available else CPU)
class DeviceDataLoader():
    """Wrap a dataloader to move data to a device"""
    def __init__(self, dl, device):
        self.dl = dl
        self.device = device
        
    def __iter__(self):
        """Yield a batch of data after moving it to device"""
        for b in self.dl:
            yield to_device(b, self.device)
        
    def __len__(self):
        """Number of batches"""
        return len(self.dl)

device = torch.device('cuda' if torch.cuda.is_available else 'cpu')


# Moving data into GPU
train_dl = DeviceDataLoader(train_dl, device)
valid_dl = DeviceDataLoader(valid_dl, device)

class_to_plant     = torch.from_numpy(class_to_plant).to(device)
class_to_plant_idx = torch.from_numpy(class_to_plant_idx).reshape((-1,1)).to(device)


################### PARTE 2: DEFINIZIONE DEL MODELLO  ###################

class ResidualBlock(nn.Module):
    def __init__(self, in_channels, out_channels):
        super().__init__()
        self.pipe = nn.Sequential(
            nn.BatchNorm2d(in_channels),
            nn.Conv2d(in_channels=in_channels, out_channels=out_channels, kernel_size=3, stride=1, padding=1),
            nn.ReLU(),
            nn.Conv2d(in_channels=out_channels, out_channels=out_channels, kernel_size=3, stride=1, padding=1),
            nn.BatchNorm2d(out_channels),
        )
        self.activation = nn.ReLU() 
        
    def forward(self, x):
        out = self.pipe(x)
        out = out + x
        out = self.activation(out)
        return out

class Model(nn.Module):
    def __init__(self, channels):
        super().__init__()
        layers = [
            nn.Conv2d(in_channels=3, out_channels=8, kernel_size=3, stride=1, padding=1),
            nn.MaxPool2d(4),
            nn.ReLU(),
            nn.BatchNorm2d(8),
            nn.Conv2d(in_channels=8, out_channels=channels, kernel_size=3, stride=1, padding=1),
            nn.MaxPool2d(2),
            nn.ReLU(),
        ]
        for _ in range(5):
            layers.append(ResidualBlock(channels, channels))
        final = nn.Sequential(
            nn.Conv2d(in_channels=channels, out_channels=16, kernel_size=1, stride=1, padding=0),
            nn.MaxPool2d(2),
            nn.Flatten(),
            nn.Linear(16*16*16, num_diseases)
        )
        layers.append(final)
        self.layers = nn.Sequential(*layers)

    def forward(self, x):
        return self.layers(x)
        

################### PARTE 3: TRAINING, VALIDATION, TEST PROCEDURES  ###################

def train(channels, plant_disease_tradeoff):
    torch.cuda.empty_cache()
    history = []

    max_epochs   = 24
    max_lr       = 1e-3
    grad_clip    = 0.05
    weight_decay = 1e-4

    model = Model(channels).to(device)
    
    optimizer = torch.optim.Adam(model.parameters(), max_lr, weight_decay=weight_decay)
    scheduler = torch.optim.lr_scheduler.OneCycleLR(optimizer, max_lr, epochs=max_epochs, steps_per_epoch=len(train_dl))

    starting_time = datetime.now()


    for epoch in range(max_epochs):
        print(f'Starting epoch {epoch}/{max_epochs}')
        model.train()
        train_losses = []
        lr_history   = []

        while len(tqdm._instances) > 0:
            tqdm._instances.pop().close()
        bar = tqdm(total=len(train_dl))

        for batch in train_dl:
            images, labels = batch
            out  = model(images)

            # we have two diffferent losses
            plant_prediction = out @ class_to_plant
            plant_idx = F.embedding(labels, class_to_plant_idx)
            loss_disease = F.cross_entropy(out, labels) # Calculate loss
            loss_plant   = F.cross_entropy(plant_prediction, plant_idx.reshape(-1))
            loss = loss_disease + plant_disease_tradeoff * loss_plant 

            loss.backward()
            train_losses.append(loss)

            nn.utils.clip_grad_value_(model.parameters(), grad_clip)

            optimizer.step()
            optimizer.zero_grad()

            lr_history.append(optimizer.param_groups[0]['lr'])
            scheduler.step()

            bar.update(1)
        bar.close()

        print(f'Starting evaluation of epoch {epoch}')
        train_mean_loss    = torch.stack(train_losses).mean()
        validation_results = evaluate(model, valid_dl)
        val_mean_loss      = validation_results['eval_mean_loss']
        val_mean_accuracy  = validation_results['eval_mean_accuracy']
        val_plant_mean_loss      = validation_results['eval_plant_mean_loss']
        val_plant_mean_accuracy  = validation_results['eval_plant_mean_accuracy']

        time = datetime.now() - starting_time
        
        msg = ''
        msg += f'epoch {epoch}  --  ' 
        msg += f'time: {time}  --  '
        msg += f'train_loss: {train_mean_loss:.4f} - '
        msg += f'val_loss: {val_mean_loss:.4f} - '
        msg += f'val_accuracy: {val_mean_accuracy:.4f} - '
        msg += f'val_plant_loss: {val_plant_mean_loss:.4f} - '
        msg += f'val_plant_accuracy: {val_plant_mean_accuracy:.4f}'
        log(msg)

    return {
        'model': model,
    }



def evaluate(model, dataset):
    model.eval()
    with torch.no_grad():
        losses     = []
        accuracies = []
        plant_losses     = []
        plant_accuracies = []

        for batch in dataset:
            images, labels = batch
            out      = model(images)
            loss     =  F.cross_entropy(out, labels)
            accuracy = compute_accuracy(out, labels)

            plant_prediction = out @ class_to_plant
            plant_idx  = F.embedding(labels, class_to_plant_idx)
            plant_loss = F.cross_entropy(plant_prediction, plant_idx.reshape(-1))
            plant_accuracy = compute_accuracy(plant_prediction, plant_idx.reshape(-1))

            losses.append(loss)
            accuracies.append(accuracy)
            plant_losses.append(plant_loss)
            plant_accuracies.append(plant_accuracy)

        mean_loss     = torch.stack(losses).mean()
        mean_accuracy = torch.stack(accuracies).mean()
        mean_plant_loss     = torch.stack(plant_losses).mean()
        mean_plant_accuracy = torch.stack(plant_accuracies).mean()
    return {
        'eval_mean_loss':     mean_loss,
        'eval_mean_accuracy': mean_accuracy,
        'eval_plant_mean_loss':     mean_plant_loss,
        'eval_plant_mean_accuracy': mean_plant_accuracy
    }


# for calculating the accuracy
def compute_accuracy(outputs, labels):
    _, preds = torch.max(outputs, dim=1)
    return torch.tensor(torch.sum(preds == labels).item() / len(preds))

    

def test(model):
    test_dir = "../data/New Plant Diseases Dataset(Augmented)/test"
    test = ImageFolder(test_dir, transform=transforms.ToTensor())
    test_images = sorted(os.listdir(test_dir + '/test')) # since images in test folder are in alphabetical order
    test_dl = DataLoader(test, batch_size, num_workers=2, pin_memory=True)

    results = evaluate(model, test_dl)
    mean_loss     = results['val_mean_loss']
    mean_accuracy = results['val_mean_accuracy']

    log(f'test_mean_loss: {mean_loss:.4f}  -- test_mean_accuracy: {mean_accuracy:.4f}')

################# PARTE 4: RUN EFFETTIVA ####################

train_results = train(32, 1.0)
model = train_results['model']


#######################   RISULTATI    ######################
# epoch  0  --  time: 0:00:42.618331  --  train_loss: 2.8146 - val_loss: 0.9743 - val_accuracy: 0.7376 - val_loss: 0.6971 - val_accuracy: 0.7934
# epoch  1  --  time: 0:01:26.210655  --  train_loss: 1.4058 - val_loss: 0.5793 - val_accuracy: 0.8296 - val_loss: 0.5573 - val_accuracy: 0.8526
# epoch  2  --  time: 0:02:09.958789  --  train_loss: 1.0896 - val_loss: 0.4602 - val_accuracy: 0.8627 - val_loss: 0.5254 - val_accuracy: 0.8760
# epoch  3  --  time: 0:02:54.267826  --  train_loss: 0.9046 - val_loss: 0.3967 - val_accuracy: 0.8752 - val_loss: 0.4923 - val_accuracy: 0.8931
# epoch  4  --  time: 0:03:38.104855  --  train_loss: 0.7593 - val_loss: 0.3858 - val_accuracy: 0.8793 - val_loss: 0.4768 - val_accuracy: 0.9124
# epoch  5  --  time: 0:04:23.629757  --  train_loss: 0.6750 - val_loss: 0.6858 - val_accuracy: 0.8064 - val_loss: 0.9310 - val_accuracy: 0.8460
# epoch  6  --  time: 0:05:09.678111  --  train_loss: 0.5805 - val_loss: 0.2631 - val_accuracy: 0.9195 - val_loss: 0.4240 - val_accuracy: 0.9283
# epoch  7  --  time: 0:05:55.194372  --  train_loss: 0.4684 - val_loss: 0.2510 - val_accuracy: 0.9210 - val_loss: 0.3057 - val_accuracy: 0.9476
# epoch  8  --  time: 0:06:40.960315  --  train_loss: 0.4093 - val_loss: 0.2699 - val_accuracy: 0.9143 - val_loss: 0.3467 - val_accuracy: 0.9430
# epoch  9  --  time: 0:07:26.273641  --  train_loss: 0.3433 - val_loss: 0.2074 - val_accuracy: 0.9344 - val_loss: 0.3595 - val_accuracy: 0.9505
# epoch 10  --  time: 0:08:10.737847  --  train_loss: 0.2846 - val_loss: 0.2001 - val_accuracy: 0.9380 - val_loss: 0.4382 - val_accuracy: 0.9409
# epoch 11  --  time: 0:08:55.411943  --  train_loss: 0.2354 - val_loss: 0.1809 - val_accuracy: 0.9439 - val_loss: 0.2579 - val_accuracy: 0.9660
# epoch 12  --  time: 0:09:41.242985  --  train_loss: 0.2000 - val_loss: 0.2882 - val_accuracy: 0.9127 - val_loss: 0.3937 - val_accuracy: 0.9508
# epoch 13  --  time: 0:10:24.577909  --  train_loss: 0.1520 - val_loss: 0.2161 - val_accuracy: 0.9379 - val_loss: 0.3416 - val_accuracy: 0.9579
# epoch 14  --  time: 0:11:09.036609  --  train_loss: 0.1236 - val_loss: 0.1511 - val_accuracy: 0.9561 - val_loss: 0.3039 - val_accuracy: 0.9647
# epoch 15  --  time: 0:11:53.915279  --  train_loss: 0.0974 - val_loss: 0.1394 - val_accuracy: 0.9591 - val_loss: 0.2071 - val_accuracy: 0.9762
# epoch 16  --  time: 0:12:38.112088  --  train_loss: 0.0607 - val_loss: 0.1299 - val_accuracy: 0.9631 - val_loss: 0.2202 - val_accuracy: 0.9740
# epoch 17  --  time: 0:13:21.103251  --  train_loss: 0.0397 - val_loss: 0.1253 - val_accuracy: 0.9655 - val_loss: 0.1707 - val_accuracy: 0.9802
# epoch 18  --  time: 0:14:05.687769  --  train_loss: 0.0240 - val_loss: 0.1215 - val_accuracy: 0.9661 - val_loss: 0.1683 - val_accuracy: 0.9820
# epoch 19  --  time: 0:14:48.948653  --  train_loss: 0.0160 - val_loss: 0.1120 - val_accuracy: 0.9689 - val_loss: 0.1473 - val_accuracy: 0.9845
# epoch 20  --  time: 0:15:34.376133  --  train_loss: 0.0090 - val_loss: 0.1122 - val_accuracy: 0.9694 - val_loss: 0.1490 - val_accuracy: 0.9842
# epoch 21  --  time: 0:16:17.743761  --  train_loss: 0.0051 - val_loss: 0.1060 - val_accuracy: 0.9711 - val_loss: 0.1379 - val_accuracy: 0.9853
# epoch 22  --  time: 0:17:00.992662  --  train_loss: 0.0036 - val_loss: 0.1056 - val_accuracy: 0.9706 - val_loss: 0.1347 - val_accuracy: 0.9849
# epoch 23  --  time: 0:17:46.062275  --  train_loss: 0.0031 - val_loss: 0.1040 - val_accuracy: 0.9721 - val_loss: 0.1345 - val_accuracy: 0.9851

#######################   RISULTATI    ######################
# Total params: 251,910
# Params in Linear: 155,686
