import os
import numpy as np
import pandas as pd
import torch
import matplotlib.pyplot as plt
import torch.nn as nn
from torch.utils.data import DataLoader
from PIL import Image
import torch.nn.functional as F
import torchvision.transforms as transforms
from torchvision.datasets import ImageFolder

from datetime import datetime
from tqdm import tqdm

import kornia # qui lo facciamo per la conversione dell'immagine in lab

def log(msg):
    with open('log', 'a') as f:
        f.write(msg + '\n')
    print(msg)


while len(tqdm._instances) > 0:
    tqdm._instances.pop().close()

################### PARTE 1: CARICAMENTO DATI ####################

data_dir = "../data/New Plant Diseases Dataset(Augmented)/New Plant Diseases Dataset(Augmented)"
train_dir = data_dir + "/train"
valid_dir = data_dir + "/valid"

num_diseases = 38

# custom transform
custom_transform = transforms.Compose([
    transforms.ToTensor(),
    kornia.color.rgb_to_lab,
])

# datasets for validation and training
train = ImageFolder(train_dir, transform=custom_transform)
valid = ImageFolder(valid_dir, transform=custom_transform) 

# setting the batch size
batch_size = 32

# Setting the seed value
random_seed = 42
torch.manual_seed(random_seed)

# DataLoaders for training and validation
train_dl = DataLoader(train, batch_size, shuffle=True, num_workers=6, pin_memory=True)
valid_dl = DataLoader(valid, batch_size,               num_workers=6, pin_memory=True)

# for moving data to device (CPU or GPU)
def to_device(data, device):
    """Move tensor(s) to chosen device"""
    if isinstance(data, (list,tuple)):
        return [to_device(x, device) for x in data]
    return data.to(device, non_blocking=True)


# for loading in the device (GPU if available else CPU)
class DeviceDataLoader():
    """Wrap a dataloader to move data to a device"""
    def __init__(self, dl, device):
        self.dl = dl
        self.device = device
        
    def __iter__(self):
        """Yield a batch of data after moving it to device"""
        for b in self.dl:
            yield to_device(b, self.device)
        
    def __len__(self):
        """Number of batches"""
        return len(self.dl)

device = torch.device('cuda' if torch.cuda.is_available else 'cpu')


# Moving data into GPU
train_dl = DeviceDataLoader(train_dl, device)
valid_dl = DeviceDataLoader(valid_dl, device)


################### PARTE 2: DEFINIZIONE DEL MODELLO  ###################

class ResidualBlock(nn.Module):
    def __init__(self, in_channels, out_channels):
        super().__init__()
        self.pipe = nn.Sequential(
            nn.BatchNorm2d(in_channels),
            nn.Conv2d(in_channels=in_channels, out_channels=out_channels, kernel_size=3, stride=1, padding=1),
            nn.ReLU(),
            nn.Conv2d(in_channels=out_channels, out_channels=out_channels, kernel_size=3, stride=1, padding=1),
            nn.BatchNorm2d(out_channels),
        )
        self.activation = nn.ReLU() 
        
    def forward(self, x):
        out = self.pipe(x)
        out = out + x
        out = self.activation(out)
        return out

class Basic_Convolution(nn.Module):
    def __init__(self, layer, out_channels):
        super().__init__()
        self.conv = layer
        self.normalization = nn.BatchNorm2d(out_channels)
        self.activation = nn.ReLU()

    def forward(self, x):
        t = self.conv(x)
        t = self.normalization(t)
        return self.activation(t)

class Path_L_Block(nn.Module):
    def __init__(self, hyperparameter_x):
        super().__init__()
        out_channels = hyperparameter_x // 2
        self.conv1   = Basic_Convolution(nn.Conv2d(in_channels=1, out_channels=out_channels, kernel_size=3, stride=2, padding=0), out_channels)
        self.conv2   = Basic_Convolution(nn.Conv2d(in_channels=out_channels, out_channels=out_channels, kernel_size=3, stride=1, padding=1), out_channels)
        self.conv3   = Basic_Convolution(nn.Conv2d(in_channels=out_channels, out_channels=hyperparameter_x, kernel_size=3, stride=1, padding=1), hyperparameter_x)
        self.pooling = nn.MaxPool2d(2)

    def forward(self, x):
        t = self.conv1(x)
        t = self.conv2(t)
        t = self.conv3(t)
        t = self.pooling(t)
        return t

class Path_AB_Block(nn.Module):
    def __init__(self, hyperparameter_x):
        super().__init__()
        out_channels = 32 - hyperparameter_x // 2
        self.conv1   = Basic_Convolution(nn.Conv2d(in_channels=2, out_channels=out_channels, kernel_size=3, stride=2, padding=0), out_channels)
        self.conv2   = Basic_Convolution(nn.Conv2d(in_channels=out_channels, out_channels=out_channels, kernel_size=3, stride=1, padding=1), out_channels)
        self.conv3   = Basic_Convolution(nn.Conv2d(in_channels=out_channels, out_channels=64 - hyperparameter_x, kernel_size=3, stride=1, padding=1), 64 - hyperparameter_x)
        self.pooling = nn.MaxPool2d(2)

    def forward(self, x):
        t = self.conv1(x)
        t = self.conv2(t)
        t = self.conv3(t)
        t = self.pooling(t)
        return t

class Model(nn.Module):
    def __init__(self, channels, hyperparameter_x):
        super().__init__()
        self.path_l  = Path_L_Block(hyperparameter_x)
        self.path_ab = Path_AB_Block(hyperparameter_x)

        layers = []
        for _ in range(5):
            layers.append(ResidualBlock(channels, channels))
        final = nn.Sequential(
            nn.Conv2d(in_channels=channels, out_channels=16, kernel_size=1, stride=1, padding=0),
            nn.MaxPool2d(2),
            nn.Flatten(),
            nn.Linear(15376, num_diseases)
        )
        layers.append(final)
        self.layers = nn.Sequential(*layers)

    def forward(self, x):
        l_channel, ab_channel = torch.split(x, [1,2], dim=1)
        l_path = self.path_l(l_channel)
        ab_path = self.path_ab(ab_channel)
        concat = torch.cat((l_path, ab_path), dim=1)
        return self.layers(concat)
        

################### PARTE 3: TRAINING, VALIDATION, TEST PROCEDURES  ###################

def train(channels):
    torch.cuda.empty_cache()
    history = []

    max_epochs   = 12
    max_lr       = 1e-3
    grad_clip    = 0.05
    weight_decay = 1e-4

    model = Model(channels, 16).to(device)

    optimizer = torch.optim.Adam(model.parameters(), max_lr, weight_decay=weight_decay)
    scheduler = torch.optim.lr_scheduler.OneCycleLR(optimizer, max_lr, epochs=max_epochs, steps_per_epoch=len(train_dl))

    starting_time = datetime.now()


    for epoch in range(max_epochs):
        print(f'Starting epoch {epoch}')
        model.train()
        train_losses = []
        lr_history   = []

        while len(tqdm._instances) > 0:
            tqdm._instances.pop().close()
        bar = tqdm(total=len(train_dl))

        for batch in train_dl:
            images, labels = batch
            out  = model(images)
            loss = F.cross_entropy(out, labels)
            loss.backward()
            train_losses.append(loss)

            nn.utils.clip_grad_value_(model.parameters(), grad_clip)

            optimizer.step()
            optimizer.zero_grad()

            lr_history.append(optimizer.param_groups[0]['lr'])
            scheduler.step()

            bar.update(1)
        bar.close()

        print(f'Starting evaluation of epoch {epoch}')
        train_mean_loss    = torch.stack(train_losses).mean()
        validation_results = evaluate(model, valid_dl)
        val_mean_loss      = validation_results['eval_mean_loss']
        val_mean_accuracy  = validation_results['eval_mean_accuracy']

        time = datetime.now() - starting_time
        
        msg = ''
        msg += f'epoch {epoch}  --  ' 
        msg += f'time: {time}  --  '
        msg += f'train_loss: {train_mean_loss:.4f} - '
        msg += f'val_loss: {val_mean_loss:.4f} - '
        msg += f'val_accuracy: {val_mean_accuracy:.4f}'
        log(msg)

    return {
        'model': model,
    }



def evaluate(model, dataset):
    model.eval()
    with torch.no_grad():
        losses     = []
        accuracies = []

        for batch in dataset:
            images, labels = batch
            out      = model(images)
            loss     = F.cross_entropy(out, labels)
            accuracy = compute_accuracy(out, labels)

            losses.append(loss)
            accuracies.append(accuracy)

        mean_loss     = torch.stack(losses).mean()
        mean_accuracy = torch.stack(accuracies).mean()
    return {
        'eval_mean_loss':     mean_loss,
        'eval_mean_accuracy': mean_accuracy
    }


# for calculating the accuracy
def compute_accuracy(outputs, labels):
    _, preds = torch.max(outputs, dim=1)
    return torch.tensor(torch.sum(preds == labels).item() / len(preds))

    

def test(model):
    test_dir = "../data/New Plant Diseases Dataset(Augmented)/test"
    test = ImageFolder(test_dir, transform=transforms.ToTensor())
    test_images = sorted(os.listdir(test_dir + '/test')) # since images in test folder are in alphabetical order
    test_dl = DataLoader(test, batch_size, num_workers=4, pin_memory=True)

    results = evaluate(model, test_dl)
    mean_loss     = results['val_mean_loss']
    mean_accuracy = results['val_mean_accuracy']

    log(f'test_mean_loss: {mean_loss:.4f}  -- test_mean_accuracy: {mean_accuracy:.4f}')

################# PARTE 4: RUN EFFETTIVA ####################

train_results = train(64)
model = train_results['model']


#######################   RISULTATI    ######################
# epoch  0  --  time: 0:08:18.309808  --  train_loss: 0.7869 - val_loss: 0.5930 - val_accuracy: 0.8301
# epoch  1  --  time: 0:11:13.757050  --  train_loss: 0.4264 - val_loss: 0.4923 - val_accuracy: 0.8631
# epoch  2  --  time: 0:14:09.543204  --  train_loss: 0.3870 - val_loss: 0.4526 - val_accuracy: 0.8927
# epoch  3  --  time: 0:17:05.137154  --  train_loss: 0.3378 - val_loss: 0.3348 - val_accuracy: 0.9178
# epoch  4  --  time: 0:20:00.324160  --  train_loss: 0.2495 - val_loss: 0.4244 - val_accuracy: 0.9226
# epoch  5  --  time: 0:22:55.540853  --  train_loss: 0.1777 - val_loss: 0.3806 - val_accuracy: 0.9313
# epoch  6  --  time: 0:25:50.872255  --  train_loss: 0.1126 - val_loss: 0.3724 - val_accuracy: 0.9393
# epoch  7  --  time: 0:28:48.182078  --  train_loss: 0.0659 - val_loss: 0.3405 - val_accuracy: 0.9470
# epoch  8  --  time: 0:31:43.745412  --  train_loss: 0.0280 - val_loss: 0.2331 - val_accuracy: 0.9590
# epoch  9  --  time: 0:34:39.283969  --  train_loss: 0.0083 - val_loss: 0.2211 - val_accuracy: 0.9604
# epoch 10  --  time: 0:37:34.583350  --  train_loss: 0.0031 - val_loss: 0.1893 - val_accuracy: 0.9653
# epoch 11  --  time: 0:40:41.232606  --  train_loss: 0.0014 - val_loss: 0.1796 - val_accuracy: 0.9663

#######################   RISULTATI    ######################
# Total params: 974,094
# Params in Linear: 584,326
